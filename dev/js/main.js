/* frameworks */
//=include ../../dist/bower_components/jquery/dist/jquery.js
//=include ../../dist/bower_components/moff/dist/moff.min.js

/* libs */
//=include lib/modernizr-custom.js

/* plugins */
//=include plugins/wow/wow.min.js
//=include plugins/scrollmagic.js

/* separate */
//=include helpers/object-fit.js
//=include separate/global.js

/* components */
//=include components/js-header.js

// the main code

window.moffConfig = {
	// Website CSS breakpoints.
	// Default values from Twitter Bootstrap.
	// No need to set xs, because of Mobile first approach.
	breakpoints: {
		sm: 768,
		md: 992,
		lg: 1200
	},
	loadOnHover: true,
	cacheLiveTime: 2000
};



new WOW().init();

// var rellax = new Rellax('.parallax');

// $(window).resize(function() {
// 	if ($(window).width() < 960) {
// 		rellax.destroy();
// 	}
// 	else {

// 	}
// });


$(window).scroll(function(){
  $('#scroll-container').each(function(){
    if ($(this).offset().top < $(window).scrollTop()) {
      var difference = $(window).scrollTop() - $(this).offset().top;
			var 
			half = (difference / 3) + 'px',
			deg = (difference / 10) ,
      transform = 'translate3d( 0, ' + '-' + half + ',0)',
      rotate = 'rotate(' + deg + 'deg)';
      $(this).find('.parallax').css('transform', transform),
      $(this).find('.parallax-rotate').css('transform', rotate);
    } 
    else {
      $(this).find('.parallax').css('transform', 'translate3d(0,0,0)');
      $(this).find('.parallax-rotate').css('rotate', '0deg');
    }
  });
});
$(window).scroll(function(){
  $('.c-invite , .c-future').each(function(){
    if ($(this).offset().top < $(window).scrollTop()) {
      var difference = $(window).scrollTop() - $(this).offset().top;
      var half = (difference / 2) + 'px',
      transform = 'translate3d( 0, ' + '-' + half + ',0)';
      $(this).find('.parallax').css('transform', transform);
    } 
    else {
      $(this).find('.parallax').css('transform', 'translate3d(0,0,0)');
    }
  });
});


$('.youtube').click(function(){
	document.getElementById('video').play();
	$('.video').addClass('opened');
})

$('.close-btn').click(function(){
	document.getElementById('video').pause();
	$('.video').removeClass('opened');
})